![alt text](Data/header.png)

# Features
- Keep your goal and Task on screen.
![alt text](Data/qPadNoteDetail.png)
# Instruction
Download x64 binary > https://github.com/xhimanshuz/qPadNote/releases
```shell
chmod a+x qPadNote-x86_64-v0.1-pre-alpha.AppImage && sudo ./qPadNote-x86_64-v0.1-pre-alpha.AppImage
```
# Build
```
cd qPadNote
qmake qPadNote.pro
make
./qPadNote
```

or Just Double Click an Run. (autorun at startup feature is to be added soon).
# Help keep this project alive
Please contribute. We only need your ♥ and support.

## Contributers
- __Himanshu Rastogi__ <hi.himanshu14@gmail.com>

### Thank you!
- Come, chat with us: #qt @ FreeNode
- Emain: hi.himanshu14@gmail.com
